package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.dto.UserDto;
import com.example.entities.User;

@Repository
public interface IUserRepository extends JpaRepository<User, Integer> {

	String queryAll = "select new com.example.dto.UserDto(t.idUser, t.name, t.email, t.password)"
					+ " from User t"
					+ " where 1=1";
	
	@Query(value = queryAll)
	public List<UserDto> getAll();
	
	@Query(value = queryAll + " and t.idUser=?1")
	public List<UserDto> getUserById(int id);
}

package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dto.PostDto;
import com.example.dto.PostDtoResponse;
import com.example.entities.Post;
import com.example.repository.IPostRepository;
import com.example.util.Mapeo;

@Service
public class PostServiceImpl implements IPostService {

	@Autowired
	IPostRepository repository;
	
	@Autowired
	private Mapeo maper;
	
	@Override
	public PostDtoResponse save(PostDto objDto) {
		Post obj = new Post();
		PostDtoResponse response = new PostDtoResponse();
		
		try {
			obj = (Post)maper.convertToEntity(objDto, obj);
			obj = repository.save(obj);
			response.setListaDto(repository.get(obj.getIdPost()));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return response;
	}

	@Override
	public PostDtoResponse getAll() {
		PostDtoResponse response = new PostDtoResponse();
		
		try {
			response.setListaDto(repository.getAll());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return response;
	}

	@Override
	public PostDtoResponse get(int id) {
		PostDtoResponse response = new PostDtoResponse();
		
		try {
			response.setListaDto(repository.get(id));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return response;
	}

	@Override
	public PostDtoResponse getByIdUser(int idUser) {
		PostDtoResponse response = new PostDtoResponse();
		
		try {
			response.setListaDto(repository.getByUser(idUser));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return response;
	}

}

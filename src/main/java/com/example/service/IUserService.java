package com.example.service;

import com.example.dto.UserDto;
import com.example.dto.UserDtoResponse;

public interface IUserService {

	public UserDtoResponse register(UserDto user);
	public UserDtoResponse getAll();
	public UserDtoResponse get(int id);
}

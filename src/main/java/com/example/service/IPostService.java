package com.example.service;

import com.example.dto.PostDto;
import com.example.dto.PostDtoResponse;

public interface IPostService {

	public PostDtoResponse save(PostDto obj);
	public PostDtoResponse getAll();
	public PostDtoResponse get(int id);
	public PostDtoResponse getByIdUser(int idUser);
}

package com.example.dto;

import java.util.ArrayList;
import java.util.List;

public class PostDtoResponse {
List<PostDto> listaDto;
	
	public PostDtoResponse() {
		super();
		listaDto = new ArrayList<PostDto>();
	}
	
	public List<PostDto> getListaDto() {
		return listaDto;
	}
	
	public void setListaDto(List<PostDto> listaDto) {
		this.listaDto = listaDto;
	}
	
	public void addListaDto(PostDto postDto) {
		this.listaDto.add(postDto);		
	}
}

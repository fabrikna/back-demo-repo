package com.example.dto;

//import java.io.Serializable;

public class UserDto implements _DTOEntity {

	private int idser;
	private String name;
	private String email;
	private String password;
	public UserDto(int idser, String name, String email, String password) {
		super();
		this.idser = idser;
		this.name = name;
		this.email = email;
		this.password = password;
	}
	public UserDto() {
		//super();
	}
	public int getIdser() {
		return idser;
	}
	public void setIdser(int idser) {
		this.idser = idser;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
}

package com.example.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.PostDto;
import com.example.dto.PostDtoResponse;
import com.example.service.IPostService;

@RestController
@RequestMapping(value = "/api/post")
public class PostApi {
	
	@Autowired
	IPostService service;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public PostDtoResponse save(@RequestBody PostDto postDto) {
		return service.save(postDto);
	}
	
	@RequestMapping(value = "/all", produces = "application/json" ,method = RequestMethod.GET)
	public PostDtoResponse getAll(@RequestBody PostDto postDto) {
		return service.getAll();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public PostDtoResponse getOne(@PathVariable("id") int id) {
		return service.get(id);
	}
	
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public PostDtoResponse getByIdUser(@PathVariable("id") int idUser) {
		return service.getByIdUser(idUser);
	}

}
